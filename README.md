# Installation

### Clone project
```
$ git clone git@gitlab.com:jimlei/classification.git`
cd classification
```

### Set local config
`cp .env .env.local`

Change this line in .env.local to match local db settings<br/>
`DATABASE_URL=mysql://user:password@host:3306/dbname`

### Install dependencies
`composer install`

### Run migrations (update DB)
`php bin/console migrate`

### Run
`php bin\console server:run`

App should now be accessible at<br/>
http://localhost:8000<br/>
http://localhost:8000/admin