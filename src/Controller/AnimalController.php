<?php

namespace App\Controller;

use App\Entity\Genus;
use App\Entity\Kingdom;
use App\Entity\Species;
use App\Repository\AnimalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class AnimalController extends AbstractController
{
    /**
     * @Route("/{kingdom}/{type}/{family}/{genus}/{species}.html", name="animal")
     * @ParamConverter("genus", options={"mapping": {"genus": "title"}})
     * @ParamConverter("species", options={"mapping": {"species": "title"}})
     */
    public function single(AnimalRepository $repo, Genus $genus, Species $species)
    {
        return $this->render('animal/single.html.twig', [
            'animal' => $repo->findOneByGenusAndSpecies($genus, $species)
        ]);
    }
}
