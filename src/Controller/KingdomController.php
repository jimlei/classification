<?php

namespace App\Controller;

use App\Entity\Kingdom;
use App\Repository\AnimalRepository;
use App\Repository\KingdomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class KingdomController extends AbstractController
{
    /**
     * @Route("/", name="kingdoms")
     */
    public function index(KingdomRepository $repo)
    {
        return $this->render('kingdom/index.html.twig', [
            'kingdoms' => $repo->findAll()
        ]);
    }

    /**
     * @Route("/{title}.html", name="kingdom")
     */
    public function kingdom(AnimalRepository $repo, Kingdom $kingdom)
    {
        return $this->render('kingdom/single.html.twig', [
            'kingdom' => $kingdom,
            'animals' => $repo->findByKingdom($kingdom)
        ]);
    }
}
