<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Family;
use App\Entity\Genus;
use App\Entity\Kingdom;
use App\Entity\Species;
use App\Entity\Type;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

final class AnimalAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('kingdom')
            ->add('type')
            ->add('family')
            ->add('genus')
            ->add('species')
            ;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
            ->add('id')
            ->add('title')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
            ->add('id', IntegerType::class, ['disabled' => true])
            ->add('title')
            ->add('kingdom', ModelType::class, [
                'class' => Kingdom::class,
                'property' => 'title',
            ])
            ->add('type', ModelType::class, [
                'class' => Type::class,
                'property' => 'title',
            ])
            ->add('family', ModelType::class, [
                'class' => Family::class,
                'property' => 'title',
            ])
            ->add('genus', ModelType::class, [
                'class' => Genus::class,
                'property' => 'title',
            ])
            ->add('species', ModelType::class, [
                'class' => Species::class,
                'property' => 'title',
            ])
            ;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->add('id')
            ->add('title')
            ->add('kingdom', EntityType::class, [
                'class' => Kingdom::class,
                'choice_label' => 'title',
            ])
            ->add('type', EntityType::class, [
                'class' => Type::class,
                'choice_label' => 'title',
            ])
            ->add('family', EntityType::class, [
                'class' => Family::class,
                'choice_label' => 'title',
            ])
            ->add('genus', EntityType::class, [
                'class' => Genus::class,
                'choice_label' => 'title',
            ])
            ->add('species', EntityType::class, [
                'class' => Species::class,
                'choice_label' => 'title',
            ])
            ;
    }
}
