<?php

namespace App\Repository;

use App\Entity\Animal;
use App\Entity\Genus;
use App\Entity\Kingdom;
use App\Entity\Species;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Animal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Animal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Animal[]    findAll()
 * @method Animal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnimalRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Animal::class);
    }

    public function findOneByGenusAndSpecies(Genus $genus, Species $species): ?Animal
    {
        return $this->createQueryBuilder('a')
            ->addSelect('k, t, f, g, s')
            ->andWhere('a.genus = ?0')
            ->andWhere('a.species = ?1')
            ->setParameters([$genus, $species])
            ->leftJoin('a.kingdom', 'k')
            ->leftJoin('a.type', 't')
            ->leftJoin('a.family', 'f')
            ->leftJoin('a.genus', 'g')
            ->leftJoin('a.species', 's')
            ->orderBy('a.title', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }
    /**
     * @return Animal[] Returns an array of Animal objects
     */
    public function findByKingdom(Kingdom $kingdom)
    {
        return $this->createQueryBuilder('a')
            ->addSelect('k, t, f, g, s')
            ->andWhere('a.kingdom = ?0')
            ->setParameters([$kingdom])
            ->leftJoin('a.kingdom', 'k')
            ->leftJoin('a.type', 't')
            ->leftJoin('a.family', 'f')
            ->leftJoin('a.genus', 'g')
            ->leftJoin('a.species', 's')
            ->orderBy('a.title', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
            ;
    }
}
